#include <LBattery.h>
#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;
char buff[256];
int nivBat; //niveau de la batterie
int charge; //pour savoir si on est en train de charger la batterie ou non
const int colorR = 255;
const int colorG = 255;
const int colorB = 255;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(2, OUTPUT);
  pinMode(13, OUTPUT);

  //Batterie
  pinMode(13, OUTPUT);
    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    
    lcd.setRGB(colorR, colorG, colorB);
    
    // Print a message to the LCD.
    lcd.print ("Battery: ");
    lcd.print(LBattery.level());
    lcd.print ("%");
    
    
    //lowBattery(); 
    delay(10);
}
void loop() {
  // put your main code here, to run repeatedly:
  sprintf(buff,"battery level = %d", LBattery.level() ); 
  Serial.println(buff);
  nivBat=LBattery.level();
  Serial.println(nivBat);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  
  if (nivBat<100) { //si le niveau de la batterie est inférieur à 100% on fait clignoter la LED
    clignoterLED();
  }
  if (nivBat<100) {
    buzz();
  
  }
  
 delay(1000); 
}
void clignoterLED() { //fonction pour faire clignoter la LED
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);              // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(100);              // wait for a second
}
void buzz() {
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
}

